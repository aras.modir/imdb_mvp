package com.example.imdb_mvp;

import android.view.View;

import com.example.imdb_mvp.pojo.IMDBPojo;
import com.example.imdb_mvp.pojo.Rating;

public class IMDBPresenter implements IMDBContract.Presenter {
    private IMDBContract.View view;
    IMDBContract.Model model = new IMDBModel();

    @Override
    public void attatchView(IMDBContract.View view) {
        this.view = view;
        model.attatchPresenter(this);
    }

    @Override
    public void searchByWord(String word) {
        model.search(word);
        view.onDataLoading();
    }

    @Override
    public void receivedDataSuccess(IMDBPojo result) {
        view.showSuccessData(result);
        view.onDataLoadingFinished();
    }

    @Override
    public void unFailure(String msg) {
        view.unFailure(msg);
        view.onDataLoadingFinished();
    }

    @Override
    public void onSelectForcast(Rating ratinglist) {
        view.showForcastData(ratinglist);
    }
}
