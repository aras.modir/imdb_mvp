package com.example.imdb_mvp;

import android.util.Log;

import com.example.imdb_mvp.pojo.IMDBPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IMDBModel implements IMDBContract.Model {
    private IMDBContract.Presenter presenter;

    @Override
    public void attatchPresenter(IMDBContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void search(String word) {
        API.getMovie().create(IMDBWebInterface.class).searchInIMDB(word, "70ad462a")
                .enqueue(new Callback<IMDBPojo>() {
                    @Override
                    public void onResponse(Call<IMDBPojo> call, Response<IMDBPojo> response) {
                        Log.d("Message", "Actor: " + response.body().getActors());
                        presenter.receivedDataSuccess(response.body());
                    }

                    @Override
                    public void onFailure(Call<IMDBPojo> call, Throwable t) {
                        presenter.unFailure(t.toString());
                    }
                });
    }
}
