package com.example.imdb_mvp;

import com.example.imdb_mvp.pojo.IMDBPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IMDBWebInterface {
    @GET("/")
    Call<IMDBPojo>
    searchInIMDB(@Query("t") String t, @Query("apikey") String apikey);
}
