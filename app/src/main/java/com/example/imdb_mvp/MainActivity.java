package com.example.imdb_mvp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.imdb_mvp.pojo.IMDBPojo;
import com.example.imdb_mvp.pojo.Rating;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements IMDBContract.View, IMDBMVPAdapter.onItemClick {
    private EditText word;
    private Button search;
    private ImageView cover;
    private TextView result;
    IMDBPresenter presenter;
    ProgressDialog dialog;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        word = findViewById(R.id.word);
        search = findViewById(R.id.search);
        cover = findViewById(R.id.cover);
        result = findViewById(R.id.result);
        recyclerView = findViewById(R.id.recyclerview);

        presenter = new IMDBPresenter();
        dialog = new ProgressDialog(this);
        dialog.setTitle("Loading");
        dialog.setMessage("Please wait");
        presenter.attatchView(this);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.searchByWord(word.getText().toString());
            }
        });

    }

    @Override
    public void showSuccessData(IMDBPojo serverResponse) {
        result.setText(serverResponse.getTitle() + " "
                + serverResponse.getDirector());

        Picasso.get().load(serverResponse.getPoster()).into(cover);

        IMDBMVPAdapter adapter = new IMDBMVPAdapter(this, serverResponse.getRatings());
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClick(this);

    }

    @Override
    public void unFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDataLoading() {
        dialog.show();
    }

    @Override
    public void onDataLoadingFinished() {
        dialog.dismiss();
    }

    @Override
    public void showForcastData(Rating ratinglist) {
        Toast.makeText(this, ratinglist.getSource(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(Rating ratinglist) {
        presenter.onSelectForcast(ratinglist);
    }
}
